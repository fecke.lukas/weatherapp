//
//  LocalData.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 20/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

final class LocalData {
    
    static func cities() -> [CityJson]? {
        guard let path = Bundle.main.path(forResource: Constants.Paths.citiesJson, ofType: Constants.Paths.json) else { return nil }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let citiesJson = try JSONDecoder().decode([CityJson].self, from: data)
            return citiesJson
        } catch {
            print(error)
            return nil
        }
    }
}
