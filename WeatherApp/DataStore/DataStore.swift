//
//  DataStore.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 19/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

final class DataStore {
    
    static func temperature(in cityName: String, completion: ((CityWeather?) -> Void)? = nil) {
        guard let completion = completion else { return }
        guard let city = city(cityName) else {
            completion(nil)
            return
        }
        DataStore.temperature(in: city.id, completion: completion)
    }
    
    static func temperature(in cityID: Int, completion: ((CityWeather?) -> Void)? = nil) {
        OpenWeatherMapRestData.currentWeather(in: cityID) { cityWeatherResponse in
            guard let completion = completion else { return }
            completion(CityWeather(cityWeatherResponse))
        }
    }
    
    static func city(_ name: String) -> City? {
        return cities()?.first(where: {$0.name == name})
    }
    
    static func city(_ id: Int) -> City? {
        return cities()?.first(where: {$0.id == id})
    }
    
    static func cities() -> [City]? {
        guard let citiesJson = LocalData.cities() else { return nil }
        return citiesJson.map { City($0) }
    }
}
