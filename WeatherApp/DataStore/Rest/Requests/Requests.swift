//
//  Requests.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 21/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

final class Requests {
    
    static func getRequest(endpoint: String, parameters: [String : Any]? = nil, headers: [String: String]?, _ completion: ((Data?) -> Void)? = nil ) {
        AlamofireRequests.getRequest(endpoint: endpoint, parameters: parameters, headers: headers, completion)
    }
    
    static func postRequest(endpoint: String, parameters: [String : Any]? = nil, headers: [String: String]?, _ completion: ((Data?) -> Void)? = nil ) {
        AlamofireRequests.postRequest(endpoint: endpoint, parameters: parameters, headers: headers, completion)
    }
    
}
