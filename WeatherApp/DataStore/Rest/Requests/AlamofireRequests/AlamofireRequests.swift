//
//  AlamofireRequests.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 21/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation
import Alamofire

final class AlamofireRequests {
    
    static func getRequest(endpoint: String, parameters: [String : Any]? = nil, headers: HTTPHeaders?, _ completion: ((Data?) -> Void)? = nil ) {
        
        Alamofire.request(endpoint, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: headers).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                guard let json = response.data else {
                    completion?(nil)
                    return
                }
                completion?(json)
            case .failure(let error):
                completion?(nil)
                print("Error:", error)
            }
        }
    }
    
    static func postRequest(endpoint: String, parameters: [String : Any]? = nil, headers: HTTPHeaders?, _ completion: ((Data?) -> Void)? = nil ) {
        
        Alamofire.request(endpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                guard let json = response.data else {
                    completion?(nil)
                    return
                }
                completion?(json)
            case .failure(let error):
                completion?(nil)
                print("Error:", error)
            }
        }
    }
}
