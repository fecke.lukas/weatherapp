//
//  OpenWeatherMapRestData.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 19/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

final class OpenWeatherMapRestData {
    
    static func currentWeather(in cityName: String, _ completion: ((CityWeatherResponse?) -> Void)? = nil) {
        let parameters = [Constants.Rest.Params.cityName: cityName]
        currentWeather(parameters: parameters)
    }
    
    static func currentWeather(in cityID: Int, _ completion: ((CityWeatherResponse?) -> Void)? = nil) {
        let parameters = [Constants.Rest.Params.cityID: cityID]
        currentWeather(parameters: parameters, completion)
    }
    
    private static func currentWeather(parameters: [String : Any]? = nil, _ completion: ((CityWeatherResponse?) -> Void)? = nil) {
        guard var parameters = parameters else { return }
        parameters.updateValue(Constants.Rest.ParamValues.apiKey, forKey: Constants.Rest.Params.apiKey)
        
        Requests.getRequest(endpoint: Constants.Rest.Endpoints.currentWeatherEndpoint, parameters: parameters, headers: nil) { data in
            guard let completion = completion else { return }
            guard let data = data else {
                completion(nil)
                return
            }
            do {
                let cityWeatherResponse = try JSONDecoder().decode(CityWeatherResponse.self, from: data)
                completion(cityWeatherResponse)
            } catch {
                completion(nil)
                print(error)
            }
        }
    }
}
