//
//  RestData.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 19/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation
import Alamofire

final class RestData {
    
    static func currentWeather(in cityName: String, _ completion: ((CityWeatherResponse?) -> Void)? = nil) {
        OpenWeatherMapRestData.currentWeather(in: cityName, completion)
    }
    
    static func currentWeather(in cityID: Int, _ completion: ((CityWeatherResponse?) -> Void)? = nil) {
        OpenWeatherMapRestData.currentWeather(in: cityID, completion)
    }
    
}
