//
//  CityWeatherResponse.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 19/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

struct CityWeatherResponse: Decodable {
    
    let name: String?
    let main: MainWeather?
    let id: Int?
    let message: String?
    let coord: Coordinates?
}

struct MainWeather: Decodable {
    let temperature: Float?

    private enum CodingKeys: String, CodingKey {
        case temperature = "temp"
    }
}

struct Coordinates: Decodable {
    let longitute: Float
    let latitude: Float
    
    private enum CodingKeys: String, CodingKey {
        case longitute = "lon"
        case latitude = "lat"
    }
}

