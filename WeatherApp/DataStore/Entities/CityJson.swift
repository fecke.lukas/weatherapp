//
//  CityJson.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 20/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

class CityJson: Decodable {
    let id: Int
    let name: String
    let country: String
    let coord: Coordinates?
}
