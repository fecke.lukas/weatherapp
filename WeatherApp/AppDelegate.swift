//
//  AppDelegate.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 18/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let mainModule = CitiesRouter.createModule(viewType: .main, city: .london())
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .red
        
        UINavigationBar.appearance().tintColor = .darkBlue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.darkBlue,
                                                            NSAttributedString.Key.font: UIFont.AppFonts.norwester(size: 25.0)!]
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = mainModule
        window?.makeKeyAndVisible()
        
        return true
    }
}

