//
//  UIFont+extension.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 21/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

extension UIFont {
    
    struct AppFonts {
        
        private static let norwesterFont = "norwester"
        
        static func norwester(size: CGFloat) -> UIFont? {
            return UIFont(name: norwesterFont, size: size)
        }
    }
}
