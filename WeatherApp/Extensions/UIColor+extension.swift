//
//  UIColor+extension.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 21/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let cloudBlue = UIColor(red: 29/255, green: 173/255, blue: 234/255, alpha: 1.0)
    static let darkBlue = UIColor(red: 24/255, green: 36/255, blue: 69/255, alpha: 1)
}


