//
//  String+extension.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 22/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

extension String {
    
    func containsSearch(_ string: String) -> Bool {
        return localizedCaseInsensitiveContains(string)
    }
}
