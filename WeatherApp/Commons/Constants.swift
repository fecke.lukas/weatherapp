//
//  Constants.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 19/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Rest {
        
        struct Endpoints {
            static let currentWeatherEndpoint = "https://api.openweathermap.org/data/2.5/weather"
        }
        
        struct Params {
            static let apiKey = "APPID"
            static let cityID = "id"
            static let cityName = "q"
        }
        
        struct ParamValues {
            static let apiKey = "5b3631cd454424c713c5d8eebc19a389"
        }
    }
    
    struct Paths {
        static let citiesJson = "city.list"
        static let json = "json"
    }
    
    private static let kelvinValue: Float = 272
    
    static func kelvinToCelsius( _ kelvin: Float) -> Int {
        return Int(kelvin - kelvinValue)
    }
    
}
