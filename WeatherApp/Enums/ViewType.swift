//
//  ViewType.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 20/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

enum ViewType {
    case main
    case cityDetail
    
    
}
