//
//  CitiesInteractor.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 18/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class CitiesInteractor: CitiesPresenterToInteractorProtocol {
    
    var presenter: CitiesInteractorToPresenterProtocol?
    var city: City?
    var cities: [City]?
    var filteredCities: [City]?
    
    init(viewType: ViewType, city: City) {
        self.city = city
        guard viewType == .main else { return }
        self.cities = DataStore.cities()?.sorted (by: { $0.name < $1.name })
    }
    
    func fetchCurrentWeather() {
        guard let city = city else { return }
        DataStore.temperature(in: city.id) { [weak self] cityWeather in
            guard let _self = self else { return }
            guard let cityWeather = cityWeather else {
                _self.presenter?.currentWeatherFetchFailed()
                return
            }
            _self.presenter?.currentWeatherFetched(cityWeather: cityWeather)
        }
    }
    
    func updateCitySearchResults(_ searchBarText: String) {
        guard let cities = cities else { return }
        filterCities(searchBarText: searchBarText, cities: cities) { [weak self] cities in
            guard let _self = self else { return }
            presenter?.updateCitySearchResults(fetchedCities: cities)
            _self.filteredCities = cities
        }
    }
    
    private func filterCities(searchBarText: String, cities: [City], _ completion: (([City]) -> Void)) {
        let filteredCities = cities.filter { $0.name.containsSearch(searchBarText) }
        completion(filteredCities)
    }
    
    func displayWeatherForCity(with index: Int) {
        guard let filteredCities = filteredCities, filteredCities.count > index else { return }
        presenter?.presentWeatherForCity(filteredCities[index])
    }
}
