//
//  CityWeather.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 19/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

class CityWeather {
    
    let temperature: Float
    let cityName: String
    
    init?(_ cityWeatherResponse: CityWeatherResponse?) {
        
        guard let cityWeatherResponse = cityWeatherResponse,
        let temperature = cityWeatherResponse.main?.temperature,
        let cityName = cityWeatherResponse.name else { return nil }
        
        self.cityName = cityName
        self.temperature = temperature
    }
}
