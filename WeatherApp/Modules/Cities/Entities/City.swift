//
//  City.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 20/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import Foundation

class City {
    
    let id: Int
    let name: String
    let country: String
    
    init(_ cityJson: CityJson) {
        self.id = cityJson.id
        self.name = cityJson.name
        self.country = cityJson.country
    }
    
    init(id: Int = -1, name: String = "NotSet", country: String = "NotSet") {
        self.id = id
        self.name = name
        self.country = country
    }
    
    class func london() -> City {
        return City(id: 2643743, name: "London", country: "GB")
    }
}
