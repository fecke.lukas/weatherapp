//
//  CitiesProtocols.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 18/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

protocol CitiesPresenterToViewProtocol: class {
    func updateFilteredCities(filteredResults: [String])
    func showWeather(cityWeather: CityWeather)
    func showError()
}

protocol CitiesInteractorToPresenterProtocol: class {
    func currentWeatherFetched(cityWeather: CityWeather)
    func updateCitySearchResults(fetchedCities: [City])
    func presentWeatherForCity(_ city: City)
    func currentWeatherFetchFailed()
}

protocol CitiesPresenterToInteractorProtocol: class {
    var presenter: CitiesInteractorToPresenterProtocol? { get set }
    func fetchCurrentWeather()
    func updateCitySearchResults(_ searchBarText: String)
    func displayWeatherForCity(with index: Int)
}

protocol CitiesViewToPresenterProtocol: class {
    var view: CitiesPresenterToViewProtocol? { get set }
    var interactor: CitiesPresenterToInteractorProtocol? { get set }
    var router: CitiesPresenterToRouterProtocol? { get set }
    func set(segmentedCollectionView: UICollectionView, contentCollectionView: UICollectionView)
    func updateView()
    func searchBarTextChanged(_ text: String)
    func citySelected(_ index: Int)
}

protocol CitiesPresenterToRouterProtocol: class {
    static func createModule(viewType: ViewType, city: City) -> UIViewController
    func presentModule(viewType: ViewType, city: City, presenter vc: UIViewController)
}
