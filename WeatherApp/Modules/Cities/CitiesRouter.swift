//
//  CitiesiewRouter.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 18/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

class CitiesRouter: CitiesPresenterToRouterProtocol{
    
    class func createModule(viewType: ViewType, city: City) -> UIViewController {
        let view = CitiesViewController.initialize(viewType: viewType)
        let presenter: CitiesViewToPresenterProtocol & CitiesInteractorToPresenterProtocol = CitiesPresenter()
        let interactor: CitiesPresenterToInteractorProtocol = CitiesInteractor(viewType: viewType, city: city)
        let router: CitiesPresenterToRouterProtocol = CitiesRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        guard viewType == .main else {
            return view
        }
        return UINavigationController(rootViewController: view)
    }
    
    func presentModule(viewType: ViewType, city: City, presenter vc: UIViewController) {
        vc.show(CitiesRouter.createModule(viewType: viewType, city: city), sender: self)
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
