//
//  CitiesPresenter.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 18/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

class CitiesPresenter: NSObject, CitiesViewToPresenterProtocol {
    
    func updateView() {
        interactor?.fetchCurrentWeather()
    }
    
    var interactor: CitiesPresenterToInteractorProtocol?
    var view: CitiesPresenterToViewProtocol?
    var router: CitiesPresenterToRouterProtocol?
    
    func set(segmentedCollectionView: UICollectionView, contentCollectionView: UICollectionView) {

    }
    
    func searchBarTextChanged(_ text: String) {
        interactor?.updateCitySearchResults(text)
    }
    
    func citySelected(_ index: Int) {
        interactor?.displayWeatherForCity(with: index)
    }
    

}

extension CitiesPresenter: CitiesInteractorToPresenterProtocol {
    
    func presentWeatherForCity(_ city: City) {
        guard let presenter = view as? UIViewController else { return }
        router?.presentModule(viewType: .cityDetail, city: city, presenter: presenter)
    }
    
    func updateCitySearchResults(fetchedCities: [City]) {
        view?.updateFilteredCities(filteredResults: fetchedCities.map { "\($0.name), \($0.country)" })
    }
    
    func currentWeatherFetched(cityWeather: CityWeather) {
        view?.showWeather(cityWeather: cityWeather)
    }
    
    func currentWeatherFetchFailed() {
        view?.showError()
    }
    
}
