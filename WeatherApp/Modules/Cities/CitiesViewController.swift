//
//  CitiesViewController.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 18/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController {
    
    var presenter: CitiesViewToPresenterProtocol?
    
    @IBOutlet weak var temperatureTitleLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!

    
    var searchController: UISearchController!
    var resultsTableController: ResultsTableController!
    
    public static func initialize(viewType: ViewType) -> CitiesViewController {
        let vc = StoryboardScene.CitiesViewController.citiesViewController.instantiate()
        vc.setup(viewType: viewType)
        return vc
    }
    
    private func setup(viewType: ViewType) {
        guard viewType == .main else { return }
        setupSearchController()
    }
    
    private func setupSearchController() {
        resultsTableController = ResultsTableController()
        resultsTableController.tableView.delegate = self
        searchController = UISearchController(searchResultsController: resultsTableController)
        searchController.searchResultsUpdater = self
        
        navigationItem.searchController = searchController
        
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = L10n.weatherApp
        definesPresentationContext = true
        navigationController?.navigationBar.isTranslucent = true
        presenter?.updateView()
    }
    
}


extension CitiesViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension CitiesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchBarText = searchController.searchBar.text, !searchBarText.isEmpty else { return }
        presenter?.searchBarTextChanged(searchBarText)
    }
}

extension CitiesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.citySelected(indexPath.row)
    }
}

extension CitiesViewController:CitiesPresenterToViewProtocol {
    
    func showWeather(cityWeather: CityWeather) {
        temperatureLabel.text = "\(Constants.kelvinToCelsius(cityWeather.temperature))°"
        cityNameLabel.text = cityWeather.cityName
    }
    
    func updateFilteredCities(filteredResults: [String]) {
        guard let resultsController = searchController.searchResultsController as? ResultsTableController else { return }
        resultsController.searchResult = filteredResults
        resultsController.tableView.reloadData()
    }
    
    func showError() {
        let alert = UIAlertController(title: "Alert", message: "Problem Fetching Weather", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
