//
//  ResultsTableController.swift
//  WeatherApp
//
//  Created by Lukáš Fečke on 20/02/2019.
//  Copyright © 2019 Lukáš Fečke. All rights reserved.
//

import UIKit

class ResultsTableController: UITableViewController {
    
    var searchResult = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searchResult.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        let index = indexPath.row
        cell.textLabel?.font = UIFont(name: "Avenir-Roman", size: 16)
        cell.textLabel?.text = searchResult[index].description
        return cell
    }
}
